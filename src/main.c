#define _DEFAULT_SOURCE

#include <stdbool.h>

#include "mem.h"
#include "mem_internals.h"

#define DEFINE_TEST(name) static bool test_##name()
#define RUN_TEST(name) {                \
    printf( #name ": Starting\n");    \
    bool status_##name = test_##name(); \
    if (status_##name) {                \
        printf( #name ": SUCCESS\n");   \
    } else {                            \
        printf( #name ": FAILED\n");    \
    }                                   \
}
#define ASSERT(x) {                                                            \
    if (!(x)) {                                                                \
        printf(" - Test failed (%s:%d, %s):\n", __FILE__, __LINE__, __func__); \
        printf("   ASSERT(%s)\n", (#x));                                       \
        return false;                                                          \
    }                                                                          \
}

inline static size_t get_block_header_size() {
    return offsetof(struct block_header, contents);
}

inline static struct block_header *get_block_from_data(void *data) {
    return data - get_block_header_size();
}

/*
Для malloc ожидается, что:
- capacity блока будет больше или равен запрашиваемому
- блок помечен как занятый
- блок после - блок сразу после, разделенный, если возможно (здесь должно быть возможно)

Для free ожидается, что:
- блок помечен как свободный
- блок смерждился с последующим, если возможно (здесь должно быть возможно)
*/
DEFINE_TEST(malloc_and_free) {
    heap_init(0);

    void *data = _malloc(123);
    struct block_header *block = get_block_from_data(data);

    ASSERT(block == HEAP_START);
    ASSERT(block->capacity.bytes >= 123);
    ASSERT(block->is_free == false);
    ASSERT(block->next == data + block->capacity.bytes);

    _free(data);

    ASSERT(block == HEAP_START);
    ASSERT(block->capacity.bytes == REGION_MIN_SIZE - get_block_header_size());
    ASSERT(block->is_free == true);
    ASSERT(block->next == NULL);

    heap_term();

    return true;
}

/*
  a  ->   b   ->  c  
  a  ->  free ->  c
  a  ->  new  ->  c
*/
DEFINE_TEST(free_one_of_several) {
    heap_init(0);

    void *a = _malloc(32);
    void *b = _malloc(32);
    void *c = _malloc(32);

    struct block_header *block_a = get_block_from_data(a);
    struct block_header *block_b = get_block_from_data(b);
    struct block_header *block_c = get_block_from_data(c);

    _free(b);

    ASSERT(block_a->next == block_b);
    ASSERT(block_b->next == block_c);

    ASSERT(block_b->is_free == true);

    void *new = _malloc(32);
    struct block_header *block_new = get_block_from_data(new);

    ASSERT(block_a->next == block_new);
    ASSERT(block_new->next == block_c);

    ASSERT(block_new->capacity.bytes == 32);
    ASSERT(block_new->is_free == false);

    heap_term();

    return true;
}

/*
Если на куче недостаточно места, чтобы выполнить запрос,
она должна расшириться.
Помимо этого, она должна попытаться расшириться последовательно - новый регион
должен выделиться сразу после последнего, если этот адрес не занят (здесь не занят).
*/
DEFINE_TEST(malloc_big_chunk) {
    heap_init(0);

    size_t request = REGION_MIN_SIZE * 2;

    void *big_boy = _malloc(request);
    struct block_header *big_boy_block = get_block_from_data(big_boy);

    debug_heap(stdout, HEAP_START);

    ASSERT(big_boy_block == HEAP_START);
    ASSERT(big_boy_block->capacity.bytes >= request);

    heap_term();

    return true;
}

/*
Как и предыдущий тест, то теперь после 1 региона есть занятая память.
Блок должен расположиться в регионе 2 - достаточно большом, чтобы его вместить.
*/
DEFINE_TEST(malloc_big_chunk_without_consecutive_regions) {
    heap_init(0);

    void *obstacle = mmap(HEAP_START + REGION_MIN_SIZE, 100, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0);

    size_t request = REGION_MIN_SIZE * 2;

    void *big_boy = _malloc(request);
    struct block_header *big_boy_block = get_block_from_data(big_boy);

    ASSERT(big_boy_block != HEAP_START);
    ASSERT(big_boy_block->capacity.bytes >= request);

    munmap(obstacle, 100);

    heap_term();

    return true;
}

int main() {
    RUN_TEST(malloc_and_free);
    RUN_TEST(free_one_of_several);
    RUN_TEST(malloc_big_chunk);
    RUN_TEST(malloc_big_chunk_without_consecutive_regions)
}
