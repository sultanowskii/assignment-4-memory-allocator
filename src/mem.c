#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define MAX(a,b) (((a) > (b)) ? (a) : (b))

void debug_block(struct block_header* b, const char* fmt, ...);
void debug(const char* fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);
extern inline bool region_is_invalid(const struct region* r);

static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd);

static bool block_is_big_enough(size_t query, struct block_header* block) {
    return block->capacity.bytes >= query;
}

static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}

/* Инициализировать блока по адресу */
static void block_init(void* restrict addr, block_size block_sz, void* restrict next) {
    *((struct block_header*)addr) = (struct block_header) {
        .next = next,
        .capacity = capacity_from_size(block_sz),
        .is_free = true
    };
}

/* Получить реальный размер региона */
static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}

/* Замаппить по адресу */
static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap((void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0);
}

/* Аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const* addr, size_t query) {
    size_t region_size = region_actual_size(query + offsetof(struct block_header, contents));

    bool extends = true;

    void *data = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

    if (data == MAP_FAILED) {
        extends = false;
        data = map_pages(addr, region_size, 0);
        if (data == MAP_FAILED) {
            return REGION_INVALID;
        }
    }

    struct region new_region = { 
        .addr = data,
        .size = region_size,
        .extends = extends
    };

    block_init(new_region.addr, (block_size) { .bytes = new_region.size }, NULL);

    return new_region;
}

/* Инициализировать кучу */
void* heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);

    if (region_is_invalid(&region)) {
        return NULL;
    }

    return region.addr;
}

/* Освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header *first_block_in_region = (struct block_header *) HEAP_START;

    while (first_block_in_region != NULL) {
        struct block_header *block = first_block_in_region;

        size_t region_size = 0;

        // цель - дойти до конца региона, не "залезая" на следующий и не "выходя" за пределы последнего
        while (block != NULL && (blocks_continuous(block, block->next) || block->next == NULL)) {
            block->is_free = true;
            region_size += size_from_capacity(block->capacity).bytes;
            block = block->next;
        }

        struct block_header* next_region_first_block = block == NULL ? NULL : block->next;

        munmap(first_block_in_region, round_pages(region_size));

        first_block_in_region = next_region_first_block;
    }
}

#define BLOCK_MIN_CAPACITY 24

/* --- Разделение блоков (если найденный свободный блок слишком большой) --- */

/* Проверить, является ли блок разделяемым*/
static bool block_splittable(struct block_header* restrict block, size_t query) {
    return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

/* Разделить блок, если он слишком большой */
static bool split_if_too_big(struct block_header* block, size_t query) {
    size_t sanitized_query = size_max(query, BLOCK_MIN_CAPACITY);

    if (block == NULL || !block_splittable(block, sanitized_query)) {
        return false;
    }

    block_capacity new_block_capacity = { .bytes = sanitized_query };
    block_size new_block_size = size_from_capacity(new_block_capacity);

    block_size remainder_block_size = { .bytes = size_from_capacity(block->capacity).bytes - new_block_size.bytes };

    void *new_block = block;
    void *remainder_block = new_block + new_block_size.bytes;

    struct block_header *next = block->next;

    block_init(new_block, new_block_size, remainder_block);
    block_init(remainder_block, remainder_block_size, next);

    return true;
}

/*  --- Слияние соседних свободных блоков --- */

/* Получить адрес сразу после блока */
static void* block_after(struct block_header const* block) {
    return (void*) (block->contents + block->capacity.bytes);
}

/* Проверить, идет ли блок snd сразу после блока fst */
static bool blocks_continuous (
    struct block_header const* fst,
    struct block_header const* snd
) {
    return (void*)snd == block_after(fst);
}

/* Проверить, слияемы ли два блока */
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

/* Попробовать слить блок со следующим */
static bool try_merge_with_next(struct block_header* block) {
    if (block == NULL || block->next == NULL || !mergeable(block, block->next)) {
        return false;
    }

    block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;

    return true;
}

/*  --- ... ecли размера кучи хватает --- */

/* Результат поиска блока*/
struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header* block;
};

/* Найти подходящий или последний блок */
static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t sz) {
    if (block == NULL) {
        return (struct block_search_result) {
            .block = NULL,
            .type = BSR_CORRUPTED,
        };
    }

    struct block_header *prev = NULL;
    struct block_header *curr = block;

    for (; curr != NULL; prev = curr, curr = curr->next) {
        if (!curr->is_free) {
            continue;
        }

        while (try_merge_with_next(curr)) {

        }

        if (!block_is_big_enough(sz, curr)) {
            continue;
        }

        return (struct block_search_result) {
            .block = curr,
            .type = BSR_FOUND_GOOD_BLOCK,
        };
    }

    struct block_header *last = prev;

    return (struct block_search_result) {
        .block = last,
        .type = BSR_REACHED_END_NOT_FOUND,
    };
}

/*
Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
Можно переиспользовать как только кучу расширили.
В случае успеха возвращается хороший блок
Во всех остальных случаях возвращается результат find_good_or_last
*/
static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block) {
    struct block_search_result result = find_good_or_last(block, query);

    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(result.block, query);
        result.block->is_free = false;
    }

    return result;
}

/* Увеличить размер кучи */
static struct block_header* grow_heap(struct block_header* restrict last, size_t query) {
    if (last == NULL) {
        return NULL;
    }

    void *desired_address = block_after(last);
    struct region new_region = alloc_region(desired_address, query);

    if (region_is_invalid(&new_region)) {
        return NULL;
    }

    last->next = new_region.addr;

    bool is_merged = try_merge_with_next(last);

    return is_merged ? last : (struct block_header *)new_region.addr;
}

/* Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
    size_t capacity_request = size_max(query, BLOCK_MIN_CAPACITY);

    struct block_search_result result = try_memalloc_existing(capacity_request, heap_start);

    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        return result.block;
    }

    if (result.block == NULL || result.type != BSR_REACHED_END_NOT_FOUND) {
        return NULL;
    }

    struct block_header *new_region = grow_heap(result.block, capacity_request);
    if (new_region == NULL) {
        return NULL;
    }

    struct block_search_result block_in_new_region_result = try_memalloc_existing(capacity_request, new_region);
    if (block_in_new_region_result.type == BSR_FOUND_GOOD_BLOCK) {
        return block_in_new_region_result.block;
    }

    return NULL;
}

/* Выделить блок с минимум указанным размером */
void* _malloc(size_t query) {
    struct block_header* const addr = memalloc(query, (struct block_header*) HEAP_START);

    if (addr == NULL) {
        return NULL;
    }

    return addr->contents;
}

/* Получить заголовок блока */
static struct block_header* block_get_header(void* contents) {
    return (struct block_header *) (((uint8_t *)contents) - offsetof(struct block_header, contents));
}

/* Освободить блок */
void _free(void* mem) {
    if (mem == NULL) {
        return;
    }

    struct block_header* header = block_get_header(mem);
    header->is_free = true;

    while (try_merge_with_next(header)) {

    }
}
